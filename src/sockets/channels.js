import io from 'socket.io-client';

import store from '../ducks';
import { add } from '../ducks/channels';

let guild = null;

store.subscribe(() => {
  ({ app: { guild } } = store.getState());
});

const socket = io('/channels', { path: '/api/socket.io' });

socket.on('new', (channel) => {
  if (channel.guild === guild) {
    store.dispatch(add(channel));
  }
});
