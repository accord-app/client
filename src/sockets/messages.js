import io from 'socket.io-client';

import { addMessages } from '../ducks/messages';
import store from '../ducks';

const socket = io('/messages', { path: '/api/socket.io' });

let channel = null;

store.subscribe(() => {
  ({ app: { channel } } = store.getState());
});

socket.on('new', (message) => {
  if (message.channel === channel) {
    store.dispatch(addMessages(message));
  }
});
