import io from 'socket.io-client';

import { add, remove, modify } from '../ducks/guilds';
import store from '../ducks';

const socket = io('/guilds', { path: '/api/socket.io' });

socket.on('new', (guild) => {
  store.dispatch(add(guild));
});

socket.on('remove', (id) => {
  store.dispatch(remove(id));
});

socket.on('modify', ({ id, data }) => {
  store.dispatch(modify(id, data));
});
