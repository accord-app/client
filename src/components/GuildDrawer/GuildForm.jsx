import React from 'react';
import PropTypes from 'prop-types';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

import { withStyles } from '@material-ui/core';

import handleInputChange from '../../utils/handleInputChange';

const styles = theme => ({
  submit: {
    marginTop: theme.spacing.unit * 2,
  },
});

class GuildForm extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
    submitCallback: PropTypes.func.isRequired,
    guild: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }),
  }

  static defaultProps = {
    guild: null,
  }

  state = {
    name: '',
  };

  handleChange = handleInputChange.bind(this);

  componentDidMount() {
    const { guild } = this.props;

    if (guild) {
      this.setState({
        name: guild.name,
      });
    }
  }


  handleSubmit = (event) => {
    event.preventDefault();
    const { submitCallback } = this.props;

    submitCallback(this.state);
  }


  render() {
    const { classes } = this.props;
    const { name } = this.state;

    return (
      <form onSubmit={this.handleSubmit}>
        <TextField
          name="name"
          label="Name"
          fullWidth
          value={name}
          onChange={this.handleChange}
        />
        <Button
          type="submit"
          color="secondary"
          variant="contained"
          fullWidth
          className={classes.submit}
        >
          Submit
        </Button>
      </form>
    );
  }
}

export default withStyles(styles)(GuildForm);
