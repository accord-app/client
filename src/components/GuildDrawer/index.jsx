import React from 'react';
import PropTypes from 'prop-types';

import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';

import { withStyles } from '@material-ui/core';

import Guild from './Guild';
import GuildsWrapper from './GuildsWrapper';

const styles = (theme) => {
  const drawerWidth = theme.spacing.unit * 9;

  return {
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
  };
};

class GuildDrawer extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,

    guilds: PropTypes.objectOf(PropTypes.object).isRequired,

    load: PropTypes.func.isRequired,
    post: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired,
    del: PropTypes.func.isRequired,
    set: PropTypes.func.isRequired,
  }

  componentDidMount() {
    const { load } = this.props;
    load();
  }

  render() {
    const {
      classes,
      guilds,
      post, update, del, set,
    } = this.props;

    return (
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{ paper: classes.drawerPaper }}
      >
        <div className={classes.toolbar} />
        <List>
          <GuildsWrapper {...{ post, del, update }}>
            {Object.entries(guilds).map(([id, guild]) => (
              <Guild
                key={id}
                guild={guild}
                onClick={() => set(id)}
              />
            ))}
          </GuildsWrapper>
        </List>
      </Drawer>
    );
  }
}

export default withStyles(styles)(GuildDrawer);
