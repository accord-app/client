import React from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';

import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

import AddIcon from '@material-ui/icons/Add';

import { withStyles } from '@material-ui/core';

import GuildDialog from './GuildDialog';

const styles = () => ({});

class GuildsWrapper extends React.Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.arrayOf(PropTypes.node),
      PropTypes.node,
    ]).isRequired,
    del: PropTypes.func.isRequired,
    post: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired,
  };

  state = {
    guild: null,
    guildRef: null,
    dialogOpen: false,
  };

  openContextMenu = (guildComponent) => {
    this.setState({
      guild: guildComponent.props.guild,
      // TODO: Find alternative to findDOMNode
      // eslint-disable-next-line react/no-find-dom-node
      guildRef: ReactDOM.findDOMNode(guildComponent),
    });
  }

  closeContextMenu = () => {
    this.setState({
      guildRef: null,
    });
  }

  openDialogMenu = () => {
    this.setState({ dialogOpen: true });
  }

  closeDialogMenu = () => {
    this.setState({
      dialogOpen: false,
      guild: null,
    });
  }

  handleClickDelete = () => {
    const { del } = this.props;
    const { guild: { _id: guildId } } = this.state;

    del(guildId);
    this.setState({ guild: null });
    this.closeContextMenu();
  }

  handleClickEdit = () => {
    this.openDialogMenu();
    this.closeContextMenu();
  }

  render() {
    const { children, post, update } = this.props;
    const { guild, guildRef, dialogOpen } = this.state;

    return (
      <React.Fragment>
        <Menu
          anchorEl={guildRef}
          open={Boolean(guildRef)}
          onClose={this.closeContextMenu}
        >
          <MenuItem onClick={this.handleClickDelete}>Delete</MenuItem>
          <MenuItem onClick={this.handleClickEdit}>Edit</MenuItem>
        </Menu>
        <GuildDialog
          open={dialogOpen}
          onClose={this.closeDialogMenu}
          {...{ guild, post, update }}
        />
        {
          React.Children.map(
            children,
            child => React.cloneElement(child, { openContexMenu: this.openContextMenu }),
          )
        }

        <ListItem button onClick={this.openDialogMenu}>
          <ListItemAvatar>
            <Avatar>
              <AddIcon />
            </Avatar>
          </ListItemAvatar>
        </ListItem>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(GuildsWrapper);
