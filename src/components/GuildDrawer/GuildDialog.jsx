import React from 'react';
import PropTypes from 'prop-types';

import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle';

import { withStyles } from '@material-ui/core';

import GuildForm from './GuildForm';

const styles = theme => ({
  dialogPaper: {
    padding: theme.spacing.unit * 2,
  },
});

class GuildDialog extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,

    guild: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }),
    open: PropTypes.bool.isRequired,

    onClose: PropTypes.func.isRequired,
    post: PropTypes.func.isRequired,
    update: PropTypes.func.isRequired,
  }

  static defaultProps = {
    guild: null,
  }

  submitCallback = (data) => {
    const {
      guild, post, update, onClose,
    } = this.props;

    if (guild) {
      // eslint-disable-next-line no-underscore-dangle
      update(guild._id, data);
    } else {
      post(data);
    }

    onClose();
  }

  render() {
    const {
      classes, open, guild, onClose,
    } = this.props;

    return (
      <Dialog
        open={open}
        onClose={onClose}
        classes={{ paper: classes.dialogPaper }}
      >
        <DialogTitle>
          { guild ? 'Edit Guild' : 'New Guild' }
        </DialogTitle>
        <GuildForm guild={guild} submitCallback={this.submitCallback} />
      </Dialog>
    );
  }
}

export default withStyles(styles)(GuildDialog);
