import React from 'react';
import PropTypes from 'prop-types';

import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';

import { withStyles } from '@material-ui/core';

const styles = () => ({});

class Guild extends React.Component {
  static propTypes = {
    // classes: PropTypes.objectOf(PropTypes.string).isRequired,
    guild: PropTypes.shape({
      name: PropTypes.string.isRequired,
    }).isRequired,
    openContexMenu: PropTypes.func.isRequired,
    onClick: PropTypes.func.isRequired,
  };

  handleContextMenu = (event) => {
    event.preventDefault();
    const { openContexMenu } = this.props;

    openContexMenu(this);
  }

  render() {
    const { guild, onClick } = this.props;

    return (
      <ListItem
        button
        onClick={onClick}
        onContextMenu={this.handleContextMenu}
      >
        <ListItemAvatar>
          <Avatar>
            {guild.name.split(' ').map(word => word[0]).join('')}
          </Avatar>
        </ListItemAvatar>
      </ListItem>
    );
  }
}

export default withStyles(styles)(Guild);
