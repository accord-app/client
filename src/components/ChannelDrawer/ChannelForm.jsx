import React from 'react';
import PropTypes from 'prop-types';

import { withStyles, TextField, Button } from '@material-ui/core';

import handleInputChange from '../../utils/handleInputChange';

const styles = theme => ({
  submit: {
    marginTop: theme.spacing.unit * 2,
  },
});

class ChannelForm extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
    submitCallback: PropTypes.func.isRequired,
  };

  state = {
    name: '',
  };

  handleChange = handleInputChange.bind(this);

  handleSubmit = (event) => {
    event.preventDefault();
    const { submitCallback } = this.props;

    submitCallback(this.state);
  }


  render() {
    const { classes } = this.props;
    const { name } = this.state;

    return (
      <form onSubmit={this.handleSubmit}>
        <TextField
          name="name"
          label="Name"
          value={name}
          onChange={this.handleChange}
          fullWidth
        />
        <Button
          type="submit"
          color="secondary"
          variant="contained"
          fullWidth
          className={classes.submit}
        >
          Submit
        </Button>
      </form>
    );
  }
}

export default withStyles(styles)(ChannelForm);
