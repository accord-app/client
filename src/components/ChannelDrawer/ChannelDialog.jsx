import React from 'react';
import PropTypes from 'prop-types';

import { withStyles, Dialog, DialogTitle } from '@material-ui/core';

import ChannelForm from './ChannelForm';

const styles = theme => ({
  dialogPaper: {
    padding: theme.spacing.unit * 2,
  },
});

class ChannelDialog extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    create: PropTypes.func.isRequired,
  };

  submitCallback = (data) => {
    const { create } = this.props;
    create(data);
  }

  render() {
    const { classes, open, onClose } = this.props;

    return (
      <Dialog
        classes={{ paper: classes.dialogPaper }}
        {...{ open, onClose }}
      >
        <DialogTitle>Hello!</DialogTitle>
        <ChannelForm submitCallback={this.submitCallback} />
      </Dialog>
    );
  }
}

export default withStyles(styles)(ChannelDialog);
