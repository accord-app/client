import React from 'react';
import PropTypes from 'prop-types';

import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import TextField from '@material-ui/core/TextField';

import { withStyles } from '@material-ui/core';

import handleInputChange from '../../utils/handleInputChange';

const styles = () => ({
  listRoot: {
    flexGrow: 1,
    overflow: 'auto',
  },
});

class MessageList extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,

    messages: PropTypes.objectOf(PropTypes.object).isRequired,
    channelId: PropTypes.string,

    fetchMessages: PropTypes.func.isRequired,
    createMessage: PropTypes.func.isRequired,
  };

  static defaultProps = {
    channelId: null,
  }

  state = {
    inputValue: '',
  };

  handleChange = handleInputChange.bind(this);

  componentDidUpdate(prevProps) {
    const { channelId, fetchMessages } = this.props;

    if (prevProps.channelId !== channelId) {
      fetchMessages(channelId);
    }
  }

  handleSubmit = () => {
    const { createMessage, channelId } = this.props;
    const { inputValue: content } = this.state;

    createMessage(channelId, { content });
    this.setState({ inputValue: '' });
  }

  handleShiftEnter = (event) => {
    const { keyCode, shiftKey } = event;

    if (keyCode === 13 && !shiftKey) {
      event.preventDefault();
      this.handleSubmit();
    }
  }

  render() {
    const { classes, messages } = this.props;
    const { inputValue } = this.state;

    return (
      <React.Fragment>
        <List classes={{ root: classes.listRoot }}>
          {Object.entries(messages).map(([id, message]) => (
            <ListItem key={id}>
              {message.author}
:
              <br />
              {message.content.split('\n').join(<br />)}
            </ListItem>
          ))}
        </List>
        <TextField
          multiline
          fullWidth
          variant="outlined"
          name="inputValue"
          value={inputValue}
          onChange={this.handleChange}
          onKeyDown={this.handleShiftEnter}
        />
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(MessageList);
