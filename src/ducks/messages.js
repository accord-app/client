import axios from 'axios';

import { setError } from './app';
import arrToObj from '../utils/apiArrToObj';

// Action Types
const LOAD = 'discord-clone/messages/LOAD';
const ADD = 'discord-clone/messages/ADD';

const initialState = {};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LOAD: {
      return arrToObj(action.messages);
    }
    case ADD: {
      const newMessages = arrToObj(action.messages);
      return { ...state, ...newMessages };
    }
    default: {
      return { ...state };
    }
  }
}

// Action Creators
export function loadMessages(messages) {
  return {
    type: LOAD,
    messages,
  };
}

export function addMessages(...messages) {
  return {
    type: ADD,
    messages,
  };
}

// Thunks
export function fetchMessages(channelId) {
  return (dispatch) => {
    axios.get(`/api/channels/${channelId}/messages`)
      .then((res) => {
        dispatch(loadMessages(res.data));
      })
      .catch((err) => {
        dispatch(setError(err.response.data));
      });
  };
}

export function createMessage(channelId, message) {
  return (dispatch) => {
    axios.post(`/api/channels/${channelId}/messages`, message)
      .catch((err) => {
        dispatch(setError(err.response.data));
      });
  };
}
