import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import AuthComponent from '../components/Auth';

import { login, register, signout } from '../ducks/app';

const mapStateToProps = null;

const mapDispatchToProps = dispatch => bindActionCreators({
  login, register, signout,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthComponent);
