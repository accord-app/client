export default function apiArrToObj(arr) {
  return arr.reduce((combined, currObj) => (
    // eslint-disable-next-line no-underscore-dangle
    { ...combined, [currObj._id]: currObj }
  ), {});
}
